from app import app
from flask import Response, request
import json
import os
from config import allowed_types
import pandas as pd

path = os.getcwd()


@app.route("/upload", methods=['GET', 'POST'])
def upload():
    file = request.files.get('file')
    filename = file.filename
    if filename.rsplit('.', 1)[1].lower() in allowed_types:
        df = pd.read_csv(file)
        headers = list(df)
        if "Date" in headers:
            df.to_json(path+"/data.json", orient='records')
            select_options = [{"value": x, "label": x} for x in headers if not x.startswith('Date')]
            return Response(json.dumps({"results": select_options}), status=200, mimetype='application/json')
        else:
            return Response(json.dumps({"error": "file does not have date column"}), status=400, mimetype='application/json')
    else:
        return Response(json.dumps({"message": "File format error"}), status=400, mimetype='application/json')


@app.route("/choices", methods=['GET', 'POST'])
def choices():
    data = request.get_json()
    df = pd.read_json(path + "/data.json", convert_dates=False, convert_axes=False)
    resp = df[data['responses']+['Date']].to_json(orient='records')
    new_list = []
    for r in json.loads(resp):
        new_list.append({str(k): str(v) for k, v in r.items()})
    headers = []
    for head in df[data['responses']+['Date']].columns:
        headers.append({"dataField": head, "text": head})
    return Response(json.dumps({"headers": headers, "results": new_list}), status=200, mimetype='application/json')


@app.route('/graph', methods=['GET', 'POST'])
def graph():
    data = request.get_json()
    df = pd.read_json(path + "/data.json", convert_dates=False, convert_axes=False)
    date = json.loads(df[['Date']].to_json(orient='records'))
    results = df[data['responses']].to_json(orient='records')
    response_list = []
    for res in json.loads(results):
        response_list.append(formula(res))
    final_list = []
    for list1, list2 in zip(date, response_list):
        final_list.append(dict(list1, **list2))
    df = pd.DataFrame(final_list)
    df.to_json(path+"/studies_data.json", orient='records')
    return Response(json.dumps(final_list), status=200, mimetype='application/json')


def formula(data):
    studies = {}
    for x in data:
        studies[x+'_x'] = data[x]*2
        studies[x+'_y'] = data[x]*4
        studies[x+'_z'] = data[x]*6
        studies[x] = data[x]
    return studies


def moving_point_average(data, period, offset):
    studies = {}
    for x in data:
        studies[x+'_new'] = data[x]*period+offset
    return studies


@app.route("/addstudy", methods=['GET', 'POST'])
def add_study():
    try:
        data = request.get_json()
        obs = data['obs']
        period = int(data['period'])
        offset = int(data['offset'])
        study = data['study_name']
        df = pd.read_json(path + "/studies_data.json", convert_dates=False, convert_axes=False)
        date = json.loads(df[['Date']].to_json(orient='records'))
        results = df[[obs]].to_json(orient='records')
        response_list = []
        for res in json.loads(results):
            if study == 'moving_point_average':
                response_list.append(moving_point_average(res, period, offset))
            else:
                None  # add any further studies here
        final_list = []
        for list1, list2, list3 in zip(date, response_list, json.loads(df.to_json(orient='records'))):
            final_list.append(dict(list1, **list2, **list3))
        df = pd.DataFrame(final_list)
        df.to_json(path+"/studies_data.json", orient='records')
        return Response(json.dumps(final_list), status=200, mimetype='application/json')
    except Exception as e:
        return Response(json.dumps({"message": "You cannot add study right now please try again."}), status=200, mimetype='application/json')
